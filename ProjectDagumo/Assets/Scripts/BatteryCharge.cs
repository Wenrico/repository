﻿using UnityEngine;

public class BatteryCharge : MonoBehaviour {

    GameObject player;
    LightEnable lightEnable;
    public int chargeAmount = 5;
    public int chargeRange = 3;
    public int chargeInterval = 1;

    private float chargeTime;
    
    // Use this for initialization
	void Start () {
		player = PlayerMovement.GetLocalPlayerObject ();
		if (player) {
			lightEnable = player.transform.FindChild ("Flashlight").FindChild ("Flashlight-light").GetComponent<LightEnable> ();
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (!player) {
			player = PlayerMovement.GetLocalPlayerObject ();
			if (player) {
				lightEnable = player.transform.FindChild ("Flashlight").FindChild ("Flashlight-light").GetComponent<LightEnable> ();
			}
		} else {
			if (Vector3.Distance (transform.position, player.transform.position) <= chargeRange)
				chargeTime -= Time.deltaTime;

<<<<<<< HEAD
			if (chargeTime <= 0) {
				chargeTime = chargeInterval;
				lightEnable.battery += chargeAmount;
			}
		}
=======
        if (Vector3.Distance(transform.position, player.transform.position) <= chargeRange)
            chargeTime -= Time.deltaTime;

        if (chargeTime <= 0)
        {
            chargeTime = chargeInterval;
            lightEnable.battery = chargeAmount;
            lightEnable.battery += chargeAmount;
        }
>>>>>>> 00e1d8f4ba2ce56a27465761da783c375855ead9
	}
}
