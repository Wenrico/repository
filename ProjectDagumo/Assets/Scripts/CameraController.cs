﻿using UnityEngine;

public class CameraController : MonoBehaviour {

    [Tooltip("The object that the camera is targetting.")]
	public Transform CameraTarget;
    [Tooltip("Checks if the camera should teleport when an object is in the way")]
    public bool avoidObjects;
    [Tooltip("The minimum camera distance to the target.")]
    public float minCamDistance = 6;
    [Tooltip("The maximum camera distance to the target.")]
    public float maxCamDistance = 14;
    [Tooltip("The preferred distance between the camera and its target.")]
    public float preferredCamDistance = 6;
    [Tooltip("The speed at which the camera zooms in and out")]
    public float zoomSpeed = 180;
    [Tooltip("The speed at which the Camera turns when moving the mouse left and right")]
    public float mouseSensitivityX = 2;
    [Tooltip("The speed at which the Camera turns when moving the mouse up and down")]
    public float mouseSensitivityY = 1.5f;
    [Tooltip("The amount of degrees the Player can look down.")]
    public float mouseRangeDown = 60;
    [Tooltip("The amount of degrees the Player can look up.")]
    public float mouseRangeUp = 60;
    [Tooltip("The amount of seconds of inactivity it takes for the camera to start rotating around the player.")]
    public float maxInactivity = 5;
    [Tooltip("The speed at which the Camera rotates around the player when inactive.")]
    public float rotationSpeed = 0.25f;

    private float camDistance;
    private float mouseRotationX = 0;
    private float mouseRotationY = 0; //Keeps check of how many degrees the Camera has looked up or down
    private float camTargetHeight;
    private float targetDirection;
    private float notMovingTimer; //Time left before Inactivity() starts rotating the camera
    private Quaternion lastCamRotation; //Camera rotation at the previous frame
    private Vector3 lastTargetPosition; //Target position at the previous frame
    private bool notActive; //If true, the camera starts rotating around its target
    private GameObject terrain;

    void Start () {
//        camDistance = preferredCamDistance;
//        notMovingTimer = maxInactivity;
//		camTargetHeight = cameraTarget.GetComponent<MeshFilter>().mesh.bounds.size.y;
//        lastTargetPosition = cameraTarget.position;
//        terrain = GameObject.Find("Terrain");
    }

    // Camera movement happens after the main Update, so that target movement occurs first (target movement is more important)
    void LateUpdate () {
		if (CameraTarget) {
			camDistance = preferredCamDistance;
			notMovingTimer = maxInactivity;
			camTargetHeight = CameraTarget.GetComponent<MeshFilter> ().mesh.bounds.size.y;
			lastTargetPosition = CameraTarget.position;
			terrain = GameObject.Find ("Terrain");
		
			mouseRotationX += Input.GetAxis ("Mouse X") * mouseSensitivityX; //Camera moving left and right
			mouseRotationY -= Input.GetAxis ("Mouse Y") * mouseSensitivityY; //Camera moving up and down
			mouseRotationY = Mathf.Clamp (mouseRotationY, -mouseRangeDown, mouseRangeUp);

			// Zooming in and out: Calculate the distance at which the player wants his camera
			camDistance += Input.GetAxis ("Mouse ScrollWheel") * Time.deltaTime * zoomSpeed;
			camDistance = Mathf.Clamp (camDistance, minCamDistance, maxCamDistance);

			lastCamRotation = transform.rotation; //Camera angle at the last frame
			transform.rotation = Quaternion.Euler (mouseRotationY, mouseRotationX, 0);
			transform.position = CameraTarget.position - (transform.rotation * Vector3.forward * camDistance);

			// Checks if the camera is going through the floor or a wall, then corrects its position
			RaycastHit collisionHit;
			if (Physics.Linecast (CameraTarget.position, transform.position - new Vector3 (0, camTargetHeight / 2, 0), out collisionHit))
			if (avoidObjects || (collisionHit.normal.y > 0.5 && collisionHit.normal.y < 1.5))
				transform.position = collisionHit.point + new Vector3 (0, camTargetHeight / 2, 0);

			Inactivity ();
		}
	}

    void Inactivity() // If the player has not moved in a while, the camera will rotate around the player
    {
        if (!notActive)
			lastTargetPosition = CameraTarget.position;

        if (notMovingTimer <= 0 && lastCamRotation == transform.rotation)
            notActive = true;

		if (Input.GetAxis("Mouse X") != 0 || CameraTarget.position != lastTargetPosition) // Still needs some work!!
        {
            notMovingTimer = maxInactivity;
            notActive = false;
        }

        // Animation where the camera slowly rotates around its target
        if (notActive)
            mouseRotationX += rotationSpeed;

        notMovingTimer -= Time.deltaTime;
    }
}
