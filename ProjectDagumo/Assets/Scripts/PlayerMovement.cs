﻿using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(CharacterController))]
public class PlayerMovement: NetworkBehaviour {

    private CharacterController characterController;

    // Different speed settings for different movement types
    public float walkSpeed = 5;
    public float strafeSpeed = 4;
    public float sprintSpeed = 10;
    public float crouchSpeed = 2;
    public float jumpSpeed = 7.5f;

    [Tooltip("The speed at which the player turns towards the camera when moving forwards or backwards.")]
    public float turnSpeed = 8;

    private float movementSpeed;
    private float forwardSpeed;
    private float sideSpeed;
    private float characterHeight;
    private bool crouching;
    private bool canJump = true;
    private bool canDoubleJump;
    private bool forwardJump;

    private bool grabbedLedge;
    private bool checkedLedge;
    private bool camLock = true;
    private float grabHeight;
    private Collider ledgeCollider;
    private Transform ledgeTransform;

    private float verticalVelocity = 0; //The Player's current vertical velocity, influenced by gravity and jumping

    void Start () {
        Cursor.visible = true;
        characterController = GetComponent<CharacterController>();
        characterHeight = characterController.height;
    }

	public override void OnStartLocalPlayer()
	{
		GetComponent<MeshRenderer>().material.color = Color.red;
		CameraController camera = GameObject.Find("Main Camera").GetComponent<CameraController>();
		//set the variable
		camera.CameraTarget = this.transform;
	}

	public static GameObject[] GetPlayers(){
		return GameObject.FindGameObjectsWithTag ("Player");
	}

	public static GameObject GetLocalPlayerObject(){
		GameObject[] players = GameObject.FindGameObjectsWithTag ("Player");

		foreach (GameObject player in players) {
			if (player.GetComponent<PlayerMovement> ().isLocalPlayer) {
				return player;
			}
		}

		return null;
	}


    void Update()    {

		if (!isLocalPlayer) {
			return;
		}

        if (characterController.isGrounded) //Checks if the Player is currently standing on a surface
        {
            // General player movement
            forwardSpeed = Input.GetAxis("Vertical") * movementSpeed;
            sideSpeed = Input.GetAxis("Horizontal") * strafeSpeed;

            // Player jumping
            if (canJump && Input.GetButtonDown("Jump")) //Change to Input.GetButton for 'bunny hopping'
            {
                verticalVelocity = jumpSpeed;
                canDoubleJump = true;
                if (Input.GetAxis("Vertical") > 0.5) //If the character is jumping while moving forward at a reasonable speed
                    forwardJump = true;
            }
            else
                forwardJump = false;

            // Player sprinting (affects speed moving forwards and backwards)
            if (Input.GetButton("Sprint") && !crouching)
                movementSpeed = sprintSpeed;
            else
            {
                movementSpeed = walkSpeed;
                CrouchCheck(); //Checks if the player crouches
            }
        }
        else
        {
            // Movement speed restrictions when in the air
            if (!grabbedLedge)
            {
                if (forwardJump) //If the character was leaping forward when jumping, no restrictions
                    forwardSpeed = Input.GetAxis("Vertical") * movementSpeed;
                else
                    forwardSpeed = Input.GetAxis("Vertical") * movementSpeed / 2;
            }
            else
                forwardSpeed = 0;
            sideSpeed = Input.GetAxis("Horizontal") * strafeSpeed / 2;

            // Double jump while in the air
            if (canDoubleJump && verticalVelocity <= 1.5 && verticalVelocity >= -1.5 && Input.GetButtonDown("Jump"))
            {
                canDoubleJump = false;
                verticalVelocity = jumpSpeed / 1.5f;
            }
        }

        // Camera rotation
        if (!grabbedLedge && camLock) //Pans behind the target when the target is moving
        {
            float targetDirection = Mathf.LerpAngle(transform.eulerAngles.y, Camera.main.transform.eulerAngles.y, turnSpeed * Time.deltaTime);
            transform.rotation = Quaternion.Euler(transform.rotation.x, targetDirection, transform.rotation.z);
        }

        if (Input.GetKey(KeyCode.Mouse2))
            camLock = false;
        else
            camLock = true;

        verticalVelocity += Physics.gravity.y * Time.deltaTime * 1.5f;
        Vector3 velocity = transform.rotation * new Vector3(sideSpeed, verticalVelocity, forwardSpeed) * Time.deltaTime;

        // Ledge grabbing logic
        LedgeCheck();

        // Makes sure the player moves at the correct speed in the direction it is facing
        characterController.Move(velocity);
    }

    void CrouchCheck() // Player crouching
    {
        var height = characterHeight;

        if (Input.GetButtonDown("Crouch"))
            crouching = !crouching;

        if (crouching)
        {
            movementSpeed = crouchSpeed;
            height = 0.5f * characterHeight;
        }
        else
        {
            movementSpeed = walkSpeed;
            height = characterHeight;
        }

        characterController.height = Mathf.Lerp(characterController.height, height, 5 * Time.deltaTime); //Creates a smooth animation effect
    }

    void LedgeCheck()
    {
        RaycastHit hit;
        Vector3 startPosition = transform.position + new Vector3(0, characterController.height / 3, 0);
        Vector3 endPosition = startPosition + (transform.rotation * Vector3.forward * 1);
        var line = Physics.Linecast(startPosition, endPosition, out hit);
        Debug.DrawLine(startPosition, endPosition, Color.red);

        // Checks if the player is close to a ledge
        if (verticalVelocity <= 0 && line)
        {
            grabHeight = hit.transform.position.y + hit.collider.bounds.extents.y - characterController.height / 2;
            if (startPosition.y >= grabHeight - 0.2 && startPosition.y <= grabHeight + 0.2)
            {
                ledgeCollider = hit.collider;
                ledgeTransform = hit.transform;
                checkedLedge = true;
            }
        }

        // If a nearby ledge is detected, grab the ledge
        if (checkedLedge)
        {
            LedgeGrab(hit);
            if(-hit.normal != Vector3.zero)
            transform.forward = -hit.normal;
        }

        // Checks if the player is still in reach of the ledge
        if (grabbedLedge && (!ledgeCollider.bounds.Contains(endPosition) || Input.GetButtonDown("Jump") || Input.GetAxis("Vertical") < 0))
        {
            grabbedLedge = false;
            checkedLedge = false;

            if (Input.GetButtonDown("Jump"))
                verticalVelocity = jumpSpeed;
        }
    }

    void LedgeGrab(RaycastHit hit)
    {
        if (checkedLedge && verticalVelocity < 0 && transform.position.y >= grabHeight - 0.1 && transform.position.y <= grabHeight + 0.1)
        {
            grabbedLedge = true;
            transform.position = new Vector3(transform.position.x, grabHeight, transform.position.z);
        }

        if (grabbedLedge)
        {
            verticalVelocity = 5;
        }
    }
}

