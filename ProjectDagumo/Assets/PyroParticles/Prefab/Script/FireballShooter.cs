﻿using UnityEngine;

namespace DigitalRuby.PyroParticles
{
    public class FireballShooter : MonoBehaviour
    {
        public GameObject[] Prefabs;
        public double shoottimer = 3;
        private GameObject currentPrefabObject;
        private FireBaseScript currentPrefabScript;
        private Quaternion originalRotation;
        private Vector3 direction;
        public Transform player;

        private void BeginEffect()
        {
            Vector3 pos;
            float yRot = transform.rotation.eulerAngles.y;
            Vector3 forwardY = Quaternion.Euler(0.0f, yRot, 0.0f) * Vector3.forward;
            Vector3 forward = transform.forward;
            Vector3 right = transform.right;
            Vector3 up = transform.up;
            Quaternion rotation = Quaternion.identity;
            currentPrefabObject = GameObject.Instantiate(Prefabs[0]);
            currentPrefabScript = currentPrefabObject.GetComponent<FireConstantBaseScript>();

            if (currentPrefabScript == null)
            {
                // temporary effect, like a fireball
                currentPrefabScript = currentPrefabObject.GetComponent<FireBaseScript>();
                if (currentPrefabScript.IsProjectile)
                {
                    // set the start point near the player
                    direction =  player.position - transform.position;
                    rotation = Quaternion.LookRotation(direction);
                    
                    pos = transform.position + forward + right + up;
                    currentPrefabObject.transform.position = pos;
                }
                currentPrefabObject.transform.rotation = rotation;
            }
        }

        private void Update()
        {
            shoottimer -= Time.deltaTime;
            
                        
            if (shoottimer <= 0 && GameObject.Find("Allosaurus_03").GetComponent<DinoAI>().Canshoot)
            {
                shoottimer = 3;             
                BeginEffect();               
            }
        }
    }
}