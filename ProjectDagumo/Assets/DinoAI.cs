﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class DinoAI : NetworkBehaviour
{
	public GameObject dinoPrefab;

    // Public Variables
    public float fpsTargetDistance;
    public float shieldDistance;
    public float enemyLookDistance;
    public float attackDistance;
    public float enemyMovementSpeed;
    public float turnSpeed;
	private Transform player;


    // Private Variables
    private CharacterController characterController;
    private Throw shieldThrow;
    private Rigidbody rigidbody;
    private Renderer myRender;
    private Vector3 direction;
    private Animation animation;
    private bool move, shieldThrown, walking, canshoot;


    // Use this for initialization
    void Start()
    {
        characterController = GetComponent<CharacterController>();
        myRender = GetComponent<Renderer>();
        rigidbody = GetComponent<Rigidbody>();
        animation = GetComponent<Animation>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
		player = PlayerMovement.GetLocalPlayerObject ().transform;
        shieldThrow = GameObject.Find("MirrorShield").GetComponent<Throw>();
        fpsTargetDistance = Vector3.Distance(player.position, transform.position);
        shieldDistance = Vector3.Distance(shieldThrow.shieldPosition, transform.position);
        lookAtPlayer();
        moveTowards();
        if (walking)
        {
            animation.Play("Allosaurus_Walk");
        }
        else
            animation.Play("Allosaurus_Idle");
    }

    void lookAtPlayer()
    {
        if (!shieldThrow.Thrown && fpsTargetDistance < enemyLookDistance)
        {
            walking = true;
            direction = transform.position - player.position;
            canshoot = true;
        }
        else if (shieldThrow.Thrown && shieldDistance < enemyLookDistance + 10)
        {
            direction = transform.position - shieldThrow.shieldPosition;
            canshoot = false;
            walking = true;
        }
        else
        {
            canshoot = false;
            walking = false;
        }

        direction.y = 0;
        if (direction != Vector3.zero)
        transform.rotation = Quaternion.Slerp
            (transform.rotation, Quaternion.LookRotation(direction),
            turnSpeed * Time.deltaTime);
    }

    void moveTowards()
    {
        if (fpsTargetDistance <= 5)
            move = false;
        else if (fpsTargetDistance > 15)
            move = true;

            if (shieldThrow.Thrown && shieldDistance < enemyLookDistance + 10)
                transform.position = Vector3.MoveTowards(transform.position, shieldThrow.shieldPosition, 0.15f);
            else if (move && fpsTargetDistance < enemyLookDistance)
                transform.position = Vector3.MoveTowards(transform.position, player.position, 0.1f);
    }

    public bool Canshoot
    {
        get
        {
            return canshoot;
        }
    }
}