﻿using UnityEngine;
using System.Collections;
using YounGenTech.HealthScript;

public class Throw : MonoBehaviour
{
    public float Throwforce = 20, PickupDistance = 5;
    private Rigidbody swordbody;
    private CharacterController playercontroller;
    private PlayerMovement playermovement;
    private bool hasWeapon = true, preparingthrow = false;
    private float oldwalkSpeed, oldstrafeSpeed, oldcrouchSpeed, oldjumpSpeed, oldsprintSpeed;
    private GameObject player;
    private Block weaponblock;
    private Vector3 defaultpositon;
    private Quaternion defaultrotation;

    // Use this for initialization
    void Start()
    {
		GameObject player = PlayerMovement.GetLocalPlayerObject ();

		if (player) {
			playercontroller = player.GetComponent<CharacterController> ();
			playermovement = player.GetComponent<PlayerMovement> ();
			weaponblock = player.transform.FindChild ("MirrorShield").GetComponent<Block> ();
		}

        swordbody = GetComponent<Rigidbody>();
        GetComponent<BoxCollider>().enabled = false;
		defaultpositon = swordbody.transform.localPosition;
		defaultrotation = swordbody.transform.localRotation;
    }

    // Update is called once per frame
    void Update()
    {
		if (!player) {
			player = PlayerMovement.GetLocalPlayerObject ();

			if (player) {
				playercontroller = player.GetComponent<CharacterController> ();
				playermovement = player.GetComponent<PlayerMovement> ();
				weaponblock = player.transform.FindChild ("MirrorShield").GetComponent<Block> ();
			}
		} else {
			if (!weaponblock || !playercontroller || !playermovement) {
				playercontroller = player.GetComponent<CharacterController> ();
				playermovement = player.GetComponent<PlayerMovement> ();
				weaponblock = player.transform.FindChild ("MirrorShield").GetComponent<Block> ();
			} else {
				if (Input.GetKeyDown(KeyCode.Mouse0) && hasWeapon && !weaponblock.isblocking)
				{
					ZeroPlayerMovement();
					preparingthrow = true;
					this.transform.localRotation = Quaternion.Euler(270, 0, 0);
					this.transform.localPosition = new Vector3(1f, 0.24f, -0.45f);
				}
				else if (Input.GetKeyUp(KeyCode.Mouse0) && hasWeapon && preparingthrow)
				{
					swordbody.useGravity = true;
					GetComponent<BoxCollider>().enabled = true;
					transform.parent = null;
					swordbody.AddForce(new Vector3(Throwforce * playercontroller.transform.forward.x, 250, Throwforce * playercontroller.transform.forward.z));
					transform.Rotate(Vector3.forward, 120);
					hasWeapon = false;
					swordbody.freezeRotation = false;
					ReturnPlayerMovement();
					preparingthrow = false;
					player.GetComponent<Health>().Damage(new HealthEvent(player, 10));
				}
				else if (Input.GetKeyDown(KeyCode.Mouse1) && hasWeapon && preparingthrow)
				{
					ReturnPlayerMovement();
					preparingthrow = false;
				}
				else if (Input.GetKeyDown(KeyCode.E) && !hasWeapon && Vector3.Distance(player.transform.position, this.transform.position) <= PickupDistance)
				{
					Pickup();
				}
			}
		}
    }

    protected void ZeroPlayerMovement()
    {
        oldwalkSpeed = playermovement.walkSpeed;
        oldstrafeSpeed = playermovement.strafeSpeed;
        oldcrouchSpeed = playermovement.crouchSpeed;
        oldsprintSpeed = playermovement.sprintSpeed;
        oldjumpSpeed = playermovement.jumpSpeed;
        playermovement.walkSpeed = 0;
        playermovement.strafeSpeed = 0;
        playermovement.crouchSpeed = 0;
        playermovement.sprintSpeed = 0;
        playermovement.jumpSpeed = 0;
    }

    public void Pickup()
    {
        swordbody.useGravity = false;
        GetComponent<BoxCollider>().enabled = false;
        this.transform.parent = player.transform;
        this.transform.localPosition = defaultpositon;
        this.transform.localRotation = defaultrotation;
        this.swordbody.freezeRotation = true;
        this.swordbody.velocity = Vector3.zero;
        hasWeapon = true;
    }

    protected void ReturnPlayerMovement()
    {
        playermovement.walkSpeed = oldwalkSpeed;
        playermovement.strafeSpeed = oldstrafeSpeed;
        playermovement.crouchSpeed = oldcrouchSpeed;
        playermovement.sprintSpeed = oldsprintSpeed;
        playermovement.jumpSpeed = oldjumpSpeed;
    }

    public bool hasweapon
    {
        get
        {
            return hasWeapon;
        }
    }

    public Vector3 defaultpos
    {
        get
        {
            return defaultpositon;
        }
    }

    public Quaternion defaultrot
    {
        get
        {
            return defaultrot;
        }
    }

    public bool Thrown
    {
        get
        {
            return !hasweapon;
        }
    }

    public Vector3 shieldPosition
    {
        get
        {
            return swordbody.position;
        }
    }
}
