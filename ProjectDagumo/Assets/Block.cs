﻿using UnityEngine;
using System.Collections;

public class Block : MonoBehaviour {

    private bool blocking;
    private Throw shieldthrow;

    // Use this for initialization
    void Start () {
        shieldthrow = GameObject.Find("MirrorShield").GetComponent<Throw>();
    }
	
	// Update is called once per frame
	void Update () {
        if (this.transform.parent != null)
        {
            if (Input.GetKeyDown(KeyCode.Mouse1) && shieldthrow.hasweapon)
            {
                this.transform.localPosition = new Vector3(0.2f, 0.47f, 0.75f);
                this.transform.localRotation = Quaternion.Euler(-4, 340, 150);
                blocking = true;
            }
            else if(Input.GetKeyUp(KeyCode.Mouse1) && blocking)
            {
                this.transform.localPosition = new Vector3(0.67f, 0.47f, 0.21f);
                this.transform.localRotation = Quaternion.Euler(0, 340, 20);
                blocking = false;
            }
        }
	}

    public bool isblocking
    {
        get
        {
            return blocking;
        }
    }

}
