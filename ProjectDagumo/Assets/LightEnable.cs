﻿using UnityEngine;
using System.Collections;

public class LightEnable : MonoBehaviour
{
    private Light arealight;
    public float ontime = 10;
    private float resttime;

    // Use this for initialization
    void Start()
    {
        arealight = GameObject.Find("Flashlight-light").GetComponent<Light>();
        arealight.enabled = false;
        resttime = ontime;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0) && resttime > 0)
        {
            arealight.enabled = !arealight.enabled;
        }

        if (arealight.enabled)
        {
            resttime -= Time.deltaTime;
            if (resttime <= 0)
            {
                arealight.enabled = false;
            }      
        }
    }

    public float battery
    {
        get
        {
            return resttime;
        }

        set
        {
            if (value > 0)
            {
                resttime += ontime * (value / 100);
                if (resttime >= ontime)
                {
                    resttime = ontime;
                }
            }
            else if (value == 0)
            {
                resttime = 0;
            }
        }
    }

    public bool LightOn
    {
        get
        {
            return arealight.enabled;
        }
    }
}
