﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class DinoSpwan : NetworkBehaviour {

	public GameObject dinoPrefab;
	private bool hasSpwand = false;

	// Update is called once per frame
	void Update () {
		if (NetworkServer.active) {
			CmdSpawnDino ();
		} else {
			hasSpwand = false;
		}
	}

	[Command]
	void CmdSpawnDino() {
		if (!hasSpwand) {
			GameObject dino = (GameObject)Instantiate (dinoPrefab, new Vector3 (22, 0, 42), Quaternion.identity);
			NetworkServer.Spawn (dino);
			hasSpwand = true;
		} 
	}
}
