﻿using UnityEngine;
using System.Collections;

public class Firebeam : MonoBehaviour
{

    public float FlashlightAngle = 40;
    public float MaxDistance = 25;
    public bool InLight;
    private RaycastHit hit;

    private NavMeshAgent nav;
    private SphereCollider col;
    private GameObject loop;
    GameObject player;

    GameObject Flashlight;

    // Use this for initialization
    void Start()
    {
        Flashlight = GameObject.Find("Flashlight");
        loop = GameObject.Find("Loop");
        player = GameObject.Find("Player");
    }

    // Update is called once per frame

    void Update()
    {
        if(Vector3.Distance(Flashlight.transform.position, loop.transform.position) <= MaxDistance)
        {
            Vector2 playerposition = new Vector2(player.transform.position.x, player.transform.position.z);
            Vector2 loopposition = new Vector2(loop.transform.position.x, loop.transform.position.z);
            Vector2 form1 = new Vector2(playerposition.x * (player.transform.forward.x + 0.5f), playerposition.y * (player.transform.forward.z + 0.5f));
            Vector2 form2 = new Vector2(playerposition.x * (player.transform.forward.x - 0.5f), playerposition.y * (player.transform.forward.z - 0.5f));
            if (loopposition.x > playerposition.x + form2.x && loopposition.x < playerposition.x + form1.x &&
                loopposition.y > playerposition.y + form2.y && loopposition.y < playerposition.y + form1.y)
            {
                print("vuur");
            }
            if (Input.GetKeyDown(KeyCode.Y))
            {
                print("loop" + loopposition);
                print("player" + playerposition);
                print("player+1" + (playerposition + form1));
                print("player+2" + (playerposition + form2));
                print("1+2" + (form1 + form2));

            }
        }
    }
}

