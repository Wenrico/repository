﻿using UnityEngine;
using System.Collections;

public class Bucket : MonoBehaviour {

    private GameObject Player, BucketWater;
    private Vector3 oldscale;
    private bool fullbucket;

	// Use this for initialization
	void Start () {
		Player = PlayerMovement.GetLocalPlayerObject ();
        BucketWater = GameObject.Find("BucketWater");
        oldscale = BucketWater.transform.localScale;
        BucketWater.transform.localScale = Vector3.zero;
	}
	
	// Update is called once per frame
	void Update () {
		if (!Player) {
			Player = PlayerMovement.GetLocalPlayerObject ();
		} else {
			if (!fullbucket && Vector3.Distance (GameObject.Find ("WaterPool").transform.position, Player.transform.position) <= 10) {
				fullbucket = true;
				BucketWater.transform.localScale = oldscale;
			}

			if (fullbucket && Input.GetKeyDown (KeyCode.Mouse1)) {
				fullbucket = false;
				BucketWater.transform.localScale = Vector3.zero;
				if (Vector3.Distance (GameObject.Find ("Allosaurus_03").transform.position, Player.transform.position) < 5) {
                
				}
			}
		}
	}
}
