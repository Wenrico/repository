﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class Magnet : NetworkBehaviour {

    private Throw swordthrow;
    private Rigidbody swordbody;
    private Transform playertransform;
    public float speedmultiplier = 10;


    // Use this for initialization
    void Start () {
		playertransform = PlayerMovement.GetLocalPlayerObject ().transform;

		if (playertransform) {
			swordthrow = playertransform.FindChild ("MirrorShield").GetComponent<Throw> ();
			swordbody = playertransform.FindChild ("MirrorShield").GetComponent<Rigidbody> ();
		}
    }
	
	// Update is called once per frame
	void Update () {
		if (!playertransform) {
			playertransform = PlayerMovement.GetLocalPlayerObject ().transform;

			if (playertransform) {
				swordthrow = playertransform.FindChild ("MirrorShield").GetComponent<Throw> ();
				swordbody = playertransform.FindChild ("MirrorShield").GetComponent<Rigidbody> ();
			}
		} else {
			if (!swordthrow || !swordbody) {
				swordthrow = playertransform.FindChild ("MirrorShield").GetComponent<Throw> ();
				swordbody = playertransform.FindChild ("MirrorShield").GetComponent<Rigidbody> ();
			}else if (!swordthrow.hasweapon) {
				if (Input.GetKeyUp (KeyCode.Mouse0)) {
					swordbody.useGravity = true;
				} else if (Input.GetKey (KeyCode.Mouse0)) {
					float distance = Vector3.Distance (playertransform.position, swordbody.position);
					swordbody.velocity = new Vector3 (((playertransform.position.x - swordbody.position.x) / distance) * speedmultiplier, playertransform.position.y - swordbody.position.y, ((playertransform.position.z - swordbody.position.z) / distance) * speedmultiplier);
					swordbody.useGravity = false;
					if (Vector3.Distance (playertransform.position, swordbody.position) < swordthrow.PickupDistance / 2) {
						swordthrow.Pickup ();
					}
				}
			}
		}
	}
}
