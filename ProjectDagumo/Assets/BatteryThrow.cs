﻿using UnityEngine;
using System.Collections;

public class BatteryThrow : MonoBehaviour {

    public const float farthrowtime = 2, despawntime = 5, closethrowforce = 600, farthrowforce = 800;
    private CharacterController playercontroller;
    private Rigidbody batterybody;
    private Vector3 defaultscale, defaultposition;
    private Quaternion defaultrotation;
    private LightEnable flashlight;
    private bool thrown = false;

	private Transform player;

    // Use this for initialization
    void Start () {
		player = PlayerMovement.GetLocalPlayerObject ().transform;

		if (player) {
			playercontroller = player.GetComponent<CharacterController> (); //GameObject.Find("Player").GetComponent<CharacterController>();
			batterybody = GetComponent<Rigidbody>();
			defaultscale = batterybody.transform.localScale;
			defaultposition = batterybody.transform.localPosition;
			defaultrotation = batterybody.transform.localRotation;
			flashlight = player.FindChild("Flashlight").FindChild ("Flashlight-light").GetComponent<LightEnable> ();
			GetComponent<CapsuleCollider>().enabled = false;
			batterybody.transform.localScale = new Vector3(0, 0, 0);
		}
    }

    // Update is called once per frame
    float farthrowtimer;
    float despawntimer;
    bool holdingbattery;
    void Update()
    {
		if (!player) {
			player = PlayerMovement.GetLocalPlayerObject ().transform;

			if (player) {
				playercontroller = player.GetComponent<CharacterController> (); //GameObject.Find("Player").GetComponent<CharacterController>();
				batterybody = GetComponent<Rigidbody> ();
				defaultscale = batterybody.transform.localScale;
				defaultposition = batterybody.transform.localPosition;
				defaultrotation = batterybody.transform.localRotation;
				flashlight = player.FindChild ("Flashlight").FindChild ("Flashlight-light").GetComponent<LightEnable> ();
				GetComponent<CapsuleCollider> ().enabled = false;
				batterybody.transform.localScale = new Vector3 (0, 0, 0);
			}
		} else {
			farthrowtimer -= Time.deltaTime;

			if (!thrown) {
				if (Input.GetKeyDown (KeyCode.R)) {
					Togglevisible ();
					farthrowtimer = farthrowtime;
					holdingbattery = true;
					flashlight.battery = 0;
				} else if (Input.GetKeyUp (KeyCode.R)) {
					batterybody.freezeRotation = false;
					batterybody.useGravity = true;
					GetComponent<CapsuleCollider> ().enabled = true;
					transform.parent = null;
					despawntimer = despawntime;
					thrown = true;
					holdingbattery = false;


					if (farthrowtimer > 0)
						batterybody.AddForce (new Vector3 (closethrowforce * playercontroller.transform.forward.x, 40, closethrowforce * playercontroller.transform.forward.z));
					else
						batterybody.AddForce (new Vector3 (farthrowforce * playercontroller.transform.forward.x, 250 + playercontroller.velocity.y, farthrowforce * playercontroller.transform.forward.z));
				}

				if (farthrowtimer <= 0 && holdingbattery) {
					batterybody.transform.localPosition = new Vector3 (defaultposition.x, defaultposition.y + 0.5f, defaultposition.z);
				}
			}


			if (thrown) {
				batterybody.transform.Rotate (Vector3.right, 3);
				despawntimer -= Time.deltaTime;

				if (despawntimer <= 0) {
					transform.parent = player;
					batterybody.freezeRotation = true;
					batterybody.transform.localRotation = defaultrotation;
					batterybody.transform.localPosition = defaultposition;
					batterybody.transform.localScale = defaultscale;
					batterybody.useGravity = false;
					batterybody.velocity = Vector3.zero;
					GetComponent<CapsuleCollider> ().enabled = false;
					thrown = false;
					Togglevisible ();
				}

				// if (Vector3.Distance(batterybody.position, boss.position))
			}
		}
    }

    bool visible = false;
    public void Togglevisible()
    {
        if (visible)
        {
            visible = false;
            batterybody.transform.localScale = new Vector3(0, 0, 0);
        }
        else
        {
            batterybody.transform.localScale = defaultscale;
            visible = true;
        }
    }
}
